#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE SKGBASEGUIDESIGNER ::..")

PROJECT(SKGBASEGUIDESIGNER)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

IF(SKG_WEBENGINE)
    MESSAGE( STATUS "     Mode WebEngine")
    ADD_DEFINITIONS(-DSKG_WEBENGINE=${SKG_WEBENGINE})
ENDIF(SKG_WEBENGINE)
IF(SKG_WEBKIT)
    MESSAGE( STATUS "     Mode Webkit")
    ADD_DEFINITIONS(-DSKG_WEBKIT=${SKG_WEBKIT})
ENDIF(SKG_WEBKIT)

SET(skgbaseguidesigner_SRCS 
    skgwidgetcollectiondesignerplugin.cpp
    skgtabwidgetdesignerplugin.cpp
    skgtablewidgetdesignerplugin.cpp
    skgtableviewdesignerplugin.cpp
    skgfilteredtableviewdesignerplugin.cpp
    skgtreeviewdesignerplugin.cpp
    skgcomboboxdesignerplugin.cpp
    skgcolorbuttondesignerplugin.cpp
    skgwidgetselectordesignerplugin.cpp
    skgwebviewdesignerplugin.cpp
    skgzoomselectordesignerplugin.cpp
    skgcalculatoreditdesignerplugin.cpp
    skggraphicsviewdesignerplugin.cpp
    skgshowdesignerplugin.cpp
    skgtablewithgraphdesignerplugin.cpp
    skgdateeditdesignerplugin.cpp
    skgprogressbardesignerplugin.cpp
    skgperiodeditdesignerplugin.cpp
    skgsimpleperiodeditdesignerplugin.cpp
)

SET(LIBS Qt5::Designer skgbasegui)

IF(SKG_WEBENGINE)
    SET(LIBS ${LIBS} Qt5::WebEngineWidgets)
ENDIF(SKG_WEBENGINE)
IF(SKG_WEBKIT)
    SET(LIBS ${LIBS} Qt5::WebKitWidgets)
ENDIF(SKG_WEBKIT)

ADD_LIBRARY(skgbaseguidesigner SHARED ${skgbaseguidesigner_SRCS})
TARGET_LINK_LIBRARIES(skgbaseguidesigner LINK_PUBLIC ${LIBS})
GENERATE_EXPORT_HEADER(skgbaseguidesigner BASE_NAME skgbaseguidesigner)

########### install files ###############
IF(WIN32)
    INSTALL(TARGETS skgbaseguidesigner LIBRARY ARCHIVE DESTINATION ${PLUGIN_INSTALL_DIR}/designer )
ELSE(WIN32)
    INSTALL(TARGETS skgbaseguidesigner LIBRARY DESTINATION ${PLUGIN_INSTALL_DIR}/designer )
ENDIF(WIN32)
