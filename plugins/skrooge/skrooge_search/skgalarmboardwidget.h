/***************************************************************************
 * SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
#ifndef SKGALARMBOARDWIDGET_H
#define SKGALARMBOARDWIDGET_H
/** @file
* This file is Skrooge dashbord plugin for alarms.
*
* @author Stephane MANKOWSKI / Guillaume DE BURE
*/
#include "skgboardwidget.h"

class QVBoxLayout;
class QFrame;
class QAction;

/**
 * This file is Skrooge dashbord plugin for alarms
 */
class SKGAlarmBoardWidget : public SKGBoardWidget
{
    Q_OBJECT

public:
    /**
     * Default Constructor
     * @param iParent the parent widget
     * @param iDocument the document
     */
    explicit SKGAlarmBoardWidget(QWidget* iParent, SKGDocument* iDocument);

    /**
     * Default Destructor
     */
    ~SKGAlarmBoardWidget() override;

    /**
     * Get the current state
     * MUST BE OVERWRITTEN
     * @return a string containing all information needed to set the same state.
     * Could be an XML stream
     */
    QString getState() override;

    /**
     * Set the current state
     * MUST BE OVERWRITTEN
     * @param iState must be interpreted to set the state of the widget
     */
    void setState(const QString& iState) override;

private Q_SLOTS:
    void dataModified(const QString& iTableName = QString(), int iIdTransaction = 0);

private:
    Q_DISABLE_COPY(SKGAlarmBoardWidget)
    QAction* m_menuFavorite;
    QFrame* m_frame;
    QVBoxLayout* m_layout;
};

#endif  // SKGALARMBOARDWIDGET_H
