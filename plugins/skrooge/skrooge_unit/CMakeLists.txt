#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_UNIT ::..")

PROJECT(plugin_unit)
IF(SKG_WEBENGINE)
    MESSAGE( STATUS "     Mode WebEngine")
    ADD_DEFINITIONS(-DSKG_WEBENGINE=${SKG_WEBENGINE})
ENDIF(SKG_WEBENGINE)
IF(SKG_WEBKIT)
    MESSAGE( STATUS "     Mode Webkit")
    ADD_DEFINITIONS(-DSKG_WEBKIT=${SKG_WEBKIT})
ENDIF(SKG_WEBKIT)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_unit_SRCS skgunitplugin.cpp skgunitpluginwidget.cpp skgunitboardwidget.cpp)

ki18n_wrap_ui(skrooge_unit_SRCS skgunitpluginwidget_base.ui skgunitpluginwidget_pref.ui)

kconfig_add_kcfg_files(skrooge_unit_SRCS skgunit_settings.kcfgc )

ADD_LIBRARY(skrooge_unit MODULE ${skrooge_unit_SRCS})
TARGET_LINK_LIBRARIES(skrooge_unit KF5::Parts KF5::NewStuff KF5::Archive KF5::ItemViews skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_unit DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skgunit_settings.kcfg  DESTINATION  ${KDE_INSTALL_KCFGDIR} )
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-plugin-unit.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skrooge_unit.rc  DESTINATION  ${KDE_INSTALL_KXMLGUI5DIR}/skrooge_unit )
INSTALL(FILES skrooge_unit.knsrc DESTINATION ${KDE_INSTALL_KNSRCDIR})
