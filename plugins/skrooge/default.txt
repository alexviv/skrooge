<!--
/***************************************************************************
 * SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
-->
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <style type="text/css">
      body
      {
      background-color: #{{ color_normalbackground }};
      color: #{{ color_normaltext }};
      font-size : small;
      font-family : {{ font_family }};
      }

      h1
      {
      text-decoration: underline;
      color: #{{ color_activetext }};
      }

      h2
      {
      text-decoration: underline;
      color: #{{ color_inactivetext }};
      }

      .table
      {
      border: thin solid #000000;
      border-collapse: collapse;
      }

      .tabletitle
      {
      background-color: #6495ed;
      color : #FFFF33;
      font-weight : bold;
      font-size : normal;
      }

      .tabletotal
      {
      background-color: #{{ color_activebackground }};
      font-weight : bold;
      }

      tr
      {
      padding: 2px;
      }

      td
      {
      padding: 2px;
      white-space: nowrap;
      }
    </style>    
  </head>
  <body>
    <table>
      <tr>    
	<td>
	  <img src="{{ logo }}" height="128" width="128"/>
	</td>
	<td align="left">
	  <h1>{{ title_main }}</h1>
	  <small>Date: {{ current_date }}</small><br/>
	  <small>File name: {{ document.fileName }}</small><br/>
	</td>
      </tr>
    </table>
    <h2>{{ title_personal_finance_score }}</h2>
    {% include "default/personal_finance_score.html" %}
    
    <h2>{{ document|display:"f_CURRENTAMOUNT_INCOME" }} &amp; {{ document|display:"f_CURRENTAMOUNT_EXPENSE" }} *</h2>
    {% include "default/income_vs_expenditure_table.html" %}
    <img src="https://chart.apis.google.com/chart?cht=bvs&chxs=0,{{ color_normaltext }}|1,{{ color_normaltext }}&chbh=100&chxt=x,y&chxr=1,0,{{ report.income_vs_expenditure.4.3 }}&chf=bg,s,{{ color_normalbackground }}&chco={{ color_negativetext }}|{{ color_positivetext }}&chd=t:{{ report.income_vs_expenditure.2.3 }},{{ report.income_vs_expenditure.1.3 }}&chds=0,{{ report.income_vs_expenditure.4.3 }}&chs=300x200&chl={{ document|display:"f_CURRENTAMOUNT_EXPENSE" }}|{{ document|display:"f_CURRENTAMOUNT_INCOME" }}&chts={{ color_normaltext }}&chtt={{ document|display:"f_CURRENTAMOUNT_EXPENSE" }} vs {{ document|display:"f_CURRENTAMOUNT_INCOME" }}|{{ report.period }}"/>

    <h2>{{ title_budget }}</h2>
    {% include "default/budget_table.html" %}
            
    <h2>{{ title_main_categories }}</h2>
    <table>
      <tr>
	<td align="center">
	  {% include "default/categories_previous_period_table.html" %}

	  <img src="https://chart.apis.google.com/chart?cht=p3&chf=bg,s,{{ color_normalbackground }}&chco={{ color_negativetext }}&chd=t:{{ report.categories_previous_period.1.2 }},{{ report.categories_previous_period.2.2 }},{{ report.categories_previous_period.3.2 }},{{ report.categories_previous_period.4.2 }},{{ report.categories_previous_period.5.2 }}&chs=300x100&chl=1|2|3|4|5&chds=0,400000000" />
	</td>
	<td align="center">
	  {% include "default/categories_period_table.html" %}
	
	  <img src="https://chart.apis.google.com/chart?cht=p3&chf=bg,s,{{ color_normalbackground }}&chco={{ color_negativetext }}&chd=t:{{ report.categories_period.1.2 }},{{ report.categories_period.2.2 }},{{ report.categories_period.3.2 }},{{ report.categories_period.4.2 }},{{ report.categories_period.5.2 }}&chs=300x100&chl=1|2|3|4|5&chds=0,400000000" />	  
	</tr>
    </table>
    <h2>{{ title_variations }}</h2>
     {% include "default/categories_variations.html" %}
    
    <h2>{{ title_account }} *</h2>
     {% include "default/bank_table.html" %}
     <br/>
     {% include "default/account_table.html" %}
     
    <h2>{{ title_unit }} *</h2>
     {% include "default/unit_table.html" %}      
     
    <h2>{{ title_portfolio }} *</h2>
     {% include "default/portfolio.html" %}  
     {% if report.portfolio|length %}
     <img src="https://chart.apis.google.com/chart?cht=p3&chf=bg,s,{{ color_normalbackground }}&chco={{ color_negativetext }}&chd=t:{% for item in report.portfolio %}{% if forloop.first %}{% else %}{% if forloop.last %}{{ item.5 }}{% else %}{{ item.5 }},{% endif %}{% endif %}{% endfor %}&chs=300x100&chl={% for item in report.portfolio %}{% if forloop.first %}{% else %}{% if forloop.last %}{{ item.0 }}{% else %}{{ item.0 }}|{% endif %}{% endif %}{% endfor %}&chds=0,400000000" />	  
     {% endif %}
    <h2>{{ title_highlighted }}</h2>
    {% include "default/highlighted_operations.html" %}     
    
    <p><small>* {{ msg_amount_unit_date }}</small></p>
  </body>
</html>
