#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORT_CSV ::..")

PROJECT(plugin_import_csv)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_import_csv_SRCS
	skgimportplugincsv.cpp
)

ADD_LIBRARY(skrooge_import_csv MODULE ${skrooge_import_csv_SRCS})
TARGET_LINK_LIBRARIES(skrooge_import_csv KF5::Parts skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_import_csv DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-import-csv.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})

