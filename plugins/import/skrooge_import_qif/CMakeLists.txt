#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORT_QIF ::..")

PROJECT(plugin_import_qif)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_import_qif_SRCS
	skgimportpluginqif.cpp
)

ADD_LIBRARY(skrooge_import_qif MODULE ${skrooge_import_qif_SRCS})
TARGET_LINK_LIBRARIES(skrooge_import_qif KF5::Parts skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_import_qif DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-import-qif.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})

