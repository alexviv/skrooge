#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORT_GNC ::..")

PROJECT(plugin_import_gnc)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_import_gnc_SRCS
	skgimportplugingnc.cpp
)

ADD_LIBRARY(skrooge_import_gnc MODULE ${skrooge_import_gnc_SRCS})
TARGET_LINK_LIBRARIES(skrooge_import_gnc KF5::Parts KF5::Archive skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(TARGETS skrooge_import_gnc DESTINATION ${KDE_INSTALL_QTPLUGINDIR})
INSTALL(FILES ${PROJECT_SOURCE_DIR}/org.kde.skrooge-import-gnc.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})

