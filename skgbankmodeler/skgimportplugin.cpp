/***************************************************************************
 * SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
/** @file
* This file is a plugin interface default implementation.
*
* @author Stephane MANKOWSKI / Guillaume DE BURE
 */
#include "skgimportplugin.h"

SKGImportPlugin::SKGImportPlugin(QObject* iImporter)
    : KParts::Plugin(iImporter), m_importer(qobject_cast< SKGImportExportManager * >(iImporter))
{}
SKGImportPlugin::~SKGImportPlugin()
    = default;


